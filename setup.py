from setuptools import setup, find_packages

setup(
    name='deforestationdetector',
    version='1.0.0',
    url='https://gitlab.com/satelligence/deforestation-detector',
    license='MIT',
    author='Vincent Schut',
    author_email='schut@satelligence.com',
    description='Deforestation detection from Sentinel-1 timeseries data',
    packages=find_packages(),
    scripts=['deforestationdetector/deforestationdetector.py'],
    install_requires=[
        'GDAL>=3.0.0',
        'iso8601>=0.1.14',
        'numpy>=1.20.0',
        'xarray>=0.17.0',
    ],
)
