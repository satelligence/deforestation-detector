#!/usr/bin/python3
# -*- coding: utf-8 -*-
# (c) Satelligence, see LICENSE.txt.

from concurrent.futures import ThreadPoolExecutor
import itertools
import math
import os
from random import shuffle
import subprocess
import sys
import tempfile
from typing import Optional, Union, List

import numpy as np
from osgeo import gdal, gdal_array, osr

from utils.misc import worldToPixel, pixelToWorld
from utils.misc import dates_from_stack, list_take

PREDEFINED_OPTIONS = {
    'default': [
        'INTERLEAVE=BAND', 'TILED=YES', 'BIGTIFF=YES', 'COMPRESS=DEFLATE'
    ],
}


def create_dataset(
        name: str,
        width: Optional[int] = None,
        height: Optional[int] = None,
        nbands: Optional[int] = None,
        dtype: Optional[int] = None,
        dsformat: str = 'gtiff',
        options: Union[List[str], str] = 'default',
        prototype: Optional[gdal.Dataset] = None,
        xoff: int = 0,
        yoff: int = 0,
        nodata: Optional[float] = None,
        compress: Optional[str] = None,
    ) -> gdal.Dataset:
    """Create empty gdal dataset

    Args:
        name (str): name of dataset. If name is an empty string, a MEM
            dataset will be created
        width (int): width
        height (int): height
        nbands (int): number of bands
        dtype (int): gdal data type
        dsformat (str): dataset format (gdal driver name)
        options (list or str): gdal creation options
        prototype (gdal.Dataset): prototype dataset
        xoff (int): x offset in pixels of the new dataset, relative to the
            prototype dataset
        yoff (int): y offset in pixels of the new dataset, relative to the
            prototype dataset
        nodata (float): nodata value
        compress (str): compression (only for geotiff)

    Returns:
        dataset: gdal.Dataset

    Raises:
        ValueError: when neither prototype nor widht, height, nbands and dtype
            are given.
    """

    if prototype is None and not (width and height and nbands and dtype):
        raise ValueError("Should provide either a prototype dataset or"
                         "width, height, nbands and dtype.")
    if options is None:
        options = 'default'
    if not isinstance(options, list):
        options = PREDEFINED_OPTIONS[options]
    if width is None:
        width = prototype.RasterXSize
    if height is None:
        height = prototype.RasterYSize
    if nbands is None:
        nbands = prototype.RasterCount
    if dtype is None:
        dtype = prototype.GetRasterBand(1).DataType
    if compress and (dsformat == "gtiff"):
        options += ['COMPRESS=%s' % compress]
    if name != '':
        driver = gdal.GetDriverByName(dsformat)
    else:
        driver = gdal.GetDriverByName('MEM')
    dataset = driver.Create(name, width, height, nbands, dtype, options)
    if prototype:
        gdal_array.CopyDatasetInfo(prototype, dataset, xoff, yoff)
        colortable = prototype.GetRasterBand(1).GetRasterColorTable()
        if colortable is not None:
            dataset.GetRasterBand(1).SetRasterColorTable(colortable)
        if nbands == prototype.RasterCount:
            for b_idx in range(nbands):
                desc = prototype.GetRasterBand(b_idx+1).GetDescription()
                dataset.GetRasterBand(b_idx+1).SetDescription(desc)
    dataset.SetMetadataItem('AREA_OR_POINT', 'AREA')
    if nodata is not None:
        dataset.GetRasterBand(1).SetNoDataValue(nodata)
    return dataset



class StackedDataset():
    """Stack of gdal datasets, adjusted to matching resolution and projection if necessary.

    Args:
        datasets (list[gdal.Dataset]): the datasets to stack
        mode (str): which valid extent to use: 'intersect', 'union', 'median', or 'first'. Intersect
            uses the intersection of all datasets, union uses the union, median takes the median of
            each list of corner coordinates, and first will use the extent of the first dataset
            given.
        resamp (str): resampling to use, in case warping is necessary
        roi (list): override mode by giving an explicit region of interest (ulx, uly, lrx, lry)
        roi_align (bool): whether to align the roi to pixel boundaries (of the first dataset)
        prune (bool): if True, skip datasets that do not intersect with the final extent
    """
    def __init__(self, datasets, mode='intersect', resamp='near', roi=None, roi_align=True, prune=False):
        #vrt_driver = gdal.GetDriverByName('VRT')
        #self.datasets = datasets
        geotransforms = []
        widths = []
        heights = []
        ulxs = []
        ulys = []
        lrxs = []
        lrys = []
        pixelsizes_x = []
        pixelsizes_y = []
        projections = []
        matching = False
        self.filenames = []
        for ds in datasets:
            gt = ds.GetGeoTransform()
            geotransforms.append(gt)
            widths.append(ds.RasterXSize)
            heights.append(ds.RasterYSize)
            ulxs.append(gt[0])
            ulys.append(gt[3])
            lrxs.append(gt[0] + gt[1] * widths[-1])
            lrys.append(gt[3] + gt[5] * heights[-1])
            pixelsizes_x.append(gt[1])
            pixelsizes_y.append(gt[5])
            projections.append(ds.GetProjection())
        if roi:
            out_ulx, out_uly, out_lrx, out_lry = roi
            if roi_align:
                gt = datasets[0].GetGeoTransform()
                p_ulx, p_uly = worldToPixel(gt, out_ulx, out_uly)
                p_lrx, p_lry = worldToPixel(gt, out_lrx, out_lry)
                p_ulx = int(math.floor(p_ulx))
                p_uly = int(math.ceil(p_uly))
                p_lrx = int(math.ceil(p_lrx))
                p_lry = int(math.floor(p_lry))
                out_ulx, out_uly = pixelToWorld(gt, p_ulx, p_uly)
                out_lrx, out_lry = pixelToWorld(gt, p_lrx, p_lry)
        else:
            if mode=='intersect':
                out_ulx = max(ulxs)
                out_uly = min(ulys)
                out_lrx = min(lrxs)
                out_lry = max(lrys)
                if (out_ulx >= out_lrx) or (out_lry >= out_uly):
                    print('mode: intersect requested, but datasets do not intersect. Returning None.')
                    self.datasets = [None] * len(datasets)
                    self.filenames = []
                    return
            elif mode=='union':
                out_ulx = min(ulxs)
                out_uly = max(ulys)
                out_lrx = max(lrxs)
                out_lry = min(lrys)
            elif mode=='median':
                out_ulx = np.median(np.asarray(ulxs))
                out_uly = np.median(np.asarray(ulys))
                out_lrx = np.median(np.asarray(lrxs))
                out_lry = np.median(np.asarray(lrys))
            elif mode=='first':
                out_ulx = ulxs[0]
                out_uly = ulys[0]
                out_lrx = lrxs[0]
                out_lry = lrys[0]
            else:
                print('unknown mode. Use intersect or union, or specify roi as ulx, uly, lrx, lry.')
                sys.exit()
            if len(set(ulxs)) == len(set(ulys)) == len(set(lrxs)) == len(set(lrys)) == len(set(pixelsizes_x)) == len(set(pixelsizes_y)) == 1:
                matching = True
        if roi and prune:
            within = (np.array(ulxs) <= out_lrx) & \
                     (np.array(lrxs) >= out_ulx) & \
                     (np.array(ulys) >= out_lry) & \
                     (np.array(lrys) <= out_uly)
            within_idx = []
            for i in range(len(within)):
                #print within[i], ulxs[i], ulys[i], lrxs[i], lrys[i], projections[i]
                if within[i]:
                    within_idx.append(i)
            geotransforms = list_take(geotransforms, within_idx)
            widths = list_take(widths, within_idx)
            heights = list_take(heights, within_idx)
            ulxs = list_take(ulxs, within_idx)
            ulys = list_take(ulys, within_idx)
            lrxs = list_take(lrxs, within_idx)
            lrys = list_take(lrys, within_idx)
            pixelsizes_x = list_take(pixelsizes_x, within_idx)
            pixelsizes_y = list_take(pixelsizes_y, within_idx)
            projections = list_take(projections, within_idx)
            datasets = list_take(datasets, within_idx)
        if mode=='first':
            out_pixelsize_x = pixelsizes_x[0]
            out_pixelsize_y = pixelsizes_y[0]
        else:
            out_pixelsize_x = min(pixelsizes_x)
            out_pixelsize_y = max(pixelsizes_y) # assume negative pixelsize for y!
        out_dss = []
        out_w = int(math.ceil((out_lrx - out_ulx) / out_pixelsize_x))
        out_h = int(math.ceil((out_lry - out_uly) / out_pixelsize_y))
        out_gt = (out_ulx, out_pixelsize_x, 0.0, out_uly, 0.0, out_pixelsize_y)
        pr_count = 0
        unique_projections = set(projections)
        do_warp = False
        if len(unique_projections) > 1:
            do_warp = True
        same_grid = True
        if len(set(pixelsizes_x)) > 1:
            same_grid = False
        for pr in unique_projections:
            this_pr_count = len([p for p in projections if p==pr])
            #print pr, this_pr_count
            if this_pr_count > pr_count:
                pr_count = this_pr_count
                out_projection = pr
        #out_projection = projections[0]
        #print out_ulx, out_uly, out_lrx, out_lry
        #print out_w, out_h, out_pixelsize_x, out_pixelsize_y
        out_srs = osr.SpatialReference()
        out_srs.ImportFromWkt(out_projection)
        #out_srs.SetWellKnownGeogCS()
        out_proj4 = out_srs.ExportToProj4()
        if do_warp:
            print('warping:', do_warp, out_proj4)
        else:
            print('warping:', do_warp)
        print('same grid:', same_grid, set(pixelsizes_x))
        print('matching:', matching)
        if (not do_warp) and matching:
            self.datasets = datasets
            self.tmpdir = None
        else:
            #tmpdir = os.path.join('/tmp', 'gdalvrtstack_' + os.path.basename(os.tmpnam()))
            tmpdir = tempfile.mkdtemp(prefix="gdalvrtstack_", dir="/tmp")
            #os.mkdir(tmpdir)
            self.tmpdir = tmpdir
            self.filenames = []
            warp_string = ''
            if do_warp:
                warp_string = '-t_srs "%s"' % (out_proj4,)
            for ds_i in range(len(datasets)):
                in_ds = datasets[ds_i]
                in_ds_filename = in_ds.GetFileList()[0]
                #print in_ds_filename
                #vrt_ds = vrt_driver.Create('', out_w, out_h, in_ds.RasterCount, in_ds.GetRasterBand(1).DataType)
                #vrt_ds.SetGeoTransform(out_gt)
                #vrt_ds.SetProjection(out_projection)
                #gdal.ReprojectImage(in_ds, vrt_ds, None, None, gdal.GRA_NearestNeighbour)
                vrt_filename = os.path.join(tmpdir, str(ds_i) + '.vrt')
                if do_warp or (not same_grid):
                    command = 'gdalwarp -q -of vrt %s -te %.8f %.8f %.8f %.8f -tr %.8f %.8f -r %s %s %s' % (warp_string, out_ulx, out_lry, out_lrx, out_uly, out_pixelsize_x, out_pixelsize_y, resamp, in_ds_filename, vrt_filename)
                else:
                    command = 'gdal_translate -q -of vrt -projwin %.8f %.8f %.8f %.8f %s %s' % (out_ulx, out_uly, out_lrx, out_lry, in_ds_filename, vrt_filename)
                #print command
                #os.system(command)
                subprocess.call(command, shell=True)
                out_dss.append(gdal.Open(vrt_filename))
                self.filenames.append(vrt_filename)
            self.datasets = out_dss

    def __len__(self):
        """Number of datasets in this stack

        Returns:
            int: number of datasets
        """
        return len(self.datasets)

    def __getitem__(self, i):
        """Return the i'th dataset

        Args:
            i (int): index

        Returns:
            gdal.Dataset: the i'th dataset
        """
        return self.datasets[i]

    def __del__(self):
        """Cleanup
        """
        for f in self.filenames:
            os.remove(f)
        if self.tmpdir is not None:
            try:
                os.removedirs(self.tmpdir)
            except AttributeError:
                pass


def sliceDataset(dss, w_xsize=256, w_ysize=256, padding=0, pad_fill=0, skip_x=0, skip_y=0, ulx=None, uly=None, lrx=None, lry=None, roi=None, use_cache=False, random=False, noread=False, bandlist=None):
    '''Iterator over slices of a stack of datasets

    Args:
        dss (List of datasets, or StackedDataset): the datasets to cut into slices
        w_xsize (int): x size of slices
        w_ysize (int): y size of slices
        padding (int): padding to apply
        pad_fill (number): what to fill with when reading or padding outside of a dataset
        skip_x (int): pixels between slices in x direction
        skip_y (int): pixels between slices in y direction
        ulx (int): ulx offset of region of interest, in pixel coordinates
        uly (int): uly offset of region of interest, in pixel coordinates
        lrx (int): lrx offset of region of interest, in pixel coordinates
        lry (int): lry offset of region of interest, in pixel coordinates
        roi (list[float]): roi in projected coordinates (ulx, uly, lrx, lry)
        use_cache (bool): use internal cache to avoid re-reading when padding
        random (bool): iterate in random order
        noread (bool): don't read and return data, just yield slice offsets
        bandlist (list[list]): list of lists, with per dataset the bands that should be read

    Yields:
         slices of the stack of datasets, every slice is a list of numpy arrays, one array per
            dataset. A slice consists of (data, xoff, yoff, slice_number, total_number_of_slices).
    '''
    cache = None
    if random:
        use_cache=False
    try:
        n_ds = len(dss)
    except:
        dss = [dss]
        n_ds = len(dss)
    if roi:
        gt = dss[0].GetGeoTransform()
        ulx, uly = worldToPixel(gt, roi[0], roi[1])
        lrx, lry = worldToPixel(gt, roi[2], roi[3])
        ulx = int(math.floor(ulx))
        uly = int(math.ceil(uly))
        lrx = int(math.ceil(lrx))
        lry = int(math.floor(lry))
    if ulx==None:
        ulx = 0
    if uly==None:
        uly = 0
    if lrx==None:
        ds_xsize = dss[0].RasterXSize
    else:
        ds_xsize = lrx
    if lry==None:
        ds_ysize = dss[0].RasterYSize
    else:
        ds_ysize = lry
    if bandlist is None:
        bandlist = [range(ds.RasterCount) for ds in dss]
    dss_type = [gdal_array.GDALTypeCodeToNumericTypeCode(ds.GetRasterBand(1).DataType) for ds in dss]
    dss_dates = dates_from_stack(dss)
    n_tiles = 0
    for y_off in range(uly, ds_ysize, w_ysize + skip_y):
        for x_off in range(ulx, ds_xsize, w_xsize + skip_x):
            n_tiles += 1
    tile_counter = 0
    y_range = range(uly, ds_ysize, w_ysize + skip_y)
    x_range = range(ulx, ds_xsize, w_xsize + skip_x)
    yx = list(itertools.product(y_range, x_range))
    if random:
        shuffle(yx)
        use_cache = False
    for y_off, x_off in yx:
        s_ysize = min(w_ysize, ds_ysize - y_off)
        p_ysize = s_ysize + 2*padding
        s_xsize = min(w_xsize, ds_xsize - x_off)
        p_xsize = s_xsize + 2*padding
        if use_cache and (x_off > ulx):
            cache_width = max(2*padding - skip_x, 0)
            x_off_c = x_off + cache_width
        else:
            cache_width = 0
            x_off_c = x_off
        readable_x0 = min(ds_xsize, max(0, x_off_c - padding))
        readable_y0 = max(0, y_off - padding)
        readable_x1 = min(ds_xsize, x_off + w_xsize + padding)
        readable_y1 = min(ds_ysize, y_off + w_ysize + padding)
        if noread:
            yield (None, x_off, y_off, tile_counter, n_tiles)
            tile_counter += 1
            continue
        if padding > 0:
            to_pad_x0 = -min(0, x_off_c - padding)
            to_pad_y0 = -min(0, y_off - padding)
            to_pad_x1 = max(0, (x_off + s_xsize + padding) - ds_xsize)
            to_pad_y1 = max(0, (y_off + s_ysize + padding) - ds_ysize)
        else:
            to_pad_x0, to_pad_y0, to_pad_x1, to_pad_y1 = 0, 0, 0, 0
        result = []
        for ds_i in range(n_ds):
            ds = dss[ds_i]
            ds_bands = bandlist[ds_i]
            if not ds_bands:
                continue
            ds_nbands = len(ds_bands)
            ds_type = dss_type[ds_i]
            if (cache_width == 0):
                paddedSlice = np.empty((ds_nbands, p_ysize, p_xsize), ds_type)
                paddedSlice.fill(pad_fill)
            else:
                paddedSlice = np.roll(cache[ds_i], -w_xsize, axis=2)
                if (p_xsize != cache[ds_i].shape[2]):
                    paddedSlice = paddedSlice[:,:,:p_xsize]
            if readable_x0 < readable_x1:
                bands_data = []
                for bi in ds_bands:
                    bands_data.append(gdal_array.BandReadAsArray(ds.GetRasterBand(bi+1), readable_x0, readable_y0, readable_x1-readable_x0, readable_y1-readable_y0))
                unpaddedSlice = np.asarray(bands_data)
                bands_data = None
                paddedSlice[:, to_pad_y0:p_ysize-to_pad_y1, to_pad_x0 + cache_width:p_xsize-to_pad_x1] = unpaddedSlice
            if n_ds > 1:
                result.append(paddedSlice)
            else:
                result = paddedSlice
        if use_cache:
            if n_ds == 1:
                cache = [result.copy()]
            else:
                cache = [r.copy() for r in result]
        yield (result, x_off, y_off, tile_counter, n_tiles)
        tile_counter += 1


def sliceDoMp(slice_iter, mp_function, mp_function_args=(), mp_function_kwargs={}, callback=None, callback_args=(), pool=None, progress=True):
    """Apply a function (and eventually a callback) to slices of a dataset, in a multithreaded or
    multiprocess manner.

    Args:
        slice_iter (iterator): iterator that yields slices, e.g. sliceDataset.
        mp_function (callable): the function to apply to every slice
        mp_function_args (tuple): additional arguments for mp_function
        mp_function_kwargs (dict): additional keyword arguments for mp_function
        callback (callable): callback to call when mp_function returns. Will be called in a separate
            thread in a single-threaded threadpool
        callback_args (tuple): additional arguments for callback
        pool (Pool): thread/process pool to use. It not given, a threadpool with ncpu threads will
            be used
        progress (bool): display progress
    """
    from multiprocessing import cpu_count, Pool
    try:
        import multiprocessing_logging
        multiprocessing_logging.install_mp_handler()
        mplog = True
    except:
        mplog = False
    if pool is None:
        n_cpus = cpu_count()
        # c_shape = coords.shape
        if mplog:
            pool = Pool(n_cpus - 1, initializer=multiprocessing_logging.install_mp_handler)  # , maxtasksperchild=10)
        else:
            pool = Pool(n_cpus - 1)
        pool_close = True
    elif isinstance(pool, int):
        from multiprocessing import cpu_count, Pool
        n_cpus = pool
        # c_shape = coords.shape
        if mplog:
            pool = Pool(n_cpus, initializer=multiprocessing_logging.install_mp_handler)
        else:
            pool = Pool(n_cpus)  # , maxtasksperchild=10)
        pool_close = True
    else:
        n_cpus = pool._processes
        pool_close = False
    # create single threadpool for callbacks, so running a callback does not slow down scheduling new chunks
    if callback is not None:
        cb_thread = ThreadPoolExecutor(1)
    jobs = []
    finished = 0
    for part in slice_iter:
        (data, xoff, yoff, slice_no, total_no_slices) = part
        jobs.append(pool.apply_async(mp_function, args=(data, (xoff, yoff)) + mp_function_args, kwds=mp_function_kwargs))
        if progress:
            percent = (float(finished) / total_no_slices) * 100
            print('\rqueued/done/total: %i/%i/%i - %.1f%%' % (slice_no + 1, finished, total_no_slices, percent), end='')
            sys.stdout.flush()
        while len(jobs) > (n_cpus):
            unfinished = []
            for j in jobs:
                if j.ready():
                    result = j.get(9999999)
                    if (result is not None) & (callback is not None):
                        cb_thread.submit(callback, result, *callback_args)
                    finished += 1
                else:
                    unfinished.append(j)
            jobs = unfinished
    while len(jobs) > 0:
        unfinished = []
        finished_old = finished
        for j in jobs:
            if j.ready():
                result = j.get(9999999)
                if (result is not None) & (callback is not None):
                    cb_thread.submit(callback, result, *callback_args)
                finished += 1
            else:
                unfinished.append(j)
        jobs = unfinished
        if progress and (finished_old < finished):
            percent = (float(finished) / total_no_slices) * 100
            print('\rqueued/done/total: %i/%i/%i - %.1f%%' % (slice_no + 1, finished, total_no_slices, percent), end='')
            sys.stdout.flush()
            finished_old = finished

    if pool_close:
        pool.close()
        pool.join()
    cb_thread.shutdown()
    if progress:
        print()


def sliceDo(slice_iter, mp_function, mp_function_args=(), callback=None, callback_args=(), progress=True):
    """Apply a function (and eventually a callback) to slices of a dataset serially.

    Args:
        slice_iter (iterator): iterator that yields slices, e.g. sliceDataset.
        mp_function (callable): the function to apply to every slice
        mp_function_args (tuple): additional arguments for mp_function
        callback (callable): callback to call when mp_function returns.
        callback_args (tuple): additional arguments for callback
        progress (bool): display progress
    """
    finished = 0
    for part in slice_iter:
        (data, xoff, yoff, slice_no, total_no_slices) = part
        result = mp_function(data, (xoff, yoff), *mp_function_args)
        if callback:
            callback(result, *callback_args)
        if progress:
            percent = (float(slice_no + 1) / total_no_slices) * 100
            print('\r%i/%i - %.1f%%' % (slice_no + 1, total_no_slices, percent), end='')
            sys.stdout.flush()
