#!/usr/bin/python3
# -*- coding: utf-8 -*-
# (c) Satelligence, see LICENSE.txt.

import numpy as np
from osgeo import gdal_array
import xarray as xr
from xarray import DataArray

from utils.misc import multivariate_logpdf, date2mjd

def sar_defor_prob(
        observations: DataArray,
        reference_mean: DataArray,
        reference_var: DataArray,
        sardeforprob_pdf_width=2,
        sardeforprob_pdf_shift=-2,
        sardeforprob_scaling: float = 0.95,
    ) -> DataArray:
    """Calculate deforestation for Sentinel-1 VV observations given a
    reference period.
    The defaults for the keyword args seem sensible for VV data which
    was filtered with an Enhanced Lee Filter with a gaussian kernel with
    sigma=1.5.

    Args:
        observations (DataArray): Observations with features in the 'band' dimension.
            All other dimensions than 'band' are considered to be separate observations.

    Keyword Args:
        reference_mean (DataArray): If given, this will be used
            as datasource for the mean of the reference period
        reference_var (DataArray): If given, this will be used as
            datasource for the variance of the reference period
        sardeforprob_pdf_width (float or list(float)): multiplier for
            the widts of the forest and non-forest pdfs, relative to the
            std of the reference period.
            Can be a list of values, one per input band.
        sardeforprob_pdf_shift (float or list(float)): shift for the
            means of the non-forest pdf, relative to the forest pdf.
            Can be a list of values, one per input band.
        sardeforprob_scaling (float): scale the conditional non-forest
            probabilities for individual observations between
            wurdefor_obs_prob_scaling and (1.0 - wurdefor_obs_prob_scaling)

    Raises:
        RuntimeError: when input has other dimensions than x, y, and time,
        or reference data is not in correct format

    Returns:
        xr.DataArray: timeseries with deforestation probabilities
    """
    medians = reference_mean.copy()
    variances = reference_var.copy()
    obs = observations.copy()

    obs_dims = obs.dims
    obs_attrs = obs.attrs

    # create temporary dummy band dim if inputs don't have it
    if 'band' not in obs_dims:
        obs = obs.expand_dims('band')
    if 'band' not in medians.dims:
        medians = medians.expand_dims('band')
    if 'band' not in variances.dims:
        variances = variances.expand_dims('band')

    # remove all pixels where:
    # - there are not enough observations in the reference period
    #   (nobs<2 -> zero variance -> no valid normal distribution)
    # - there are no observations in the detection period

    valid = (variances > 0) & (obs.notnull().any(dim="time"))
    valid = valid.all(dim="band")
    medians = medians.where(valid, np.nan)
    variances = variances.where(valid, np.nan)
    obs = obs.where(valid, np.nan)

    valid_obs = obs.notnull().all(dim='band')


    if np.size(sardeforprob_pdf_width) > 1:
        sardeforprob_pdf_width = xr.DataArray(
            sardeforprob_pdf_width,
            dims=["band"],
            coords={"band": variances.coords["band"]},
        )

    if np.size(sardeforprob_pdf_shift) > 1:
        sardeforprob_pdf_shift = xr.DataArray(
            sardeforprob_pdf_shift,
            dims=["band"],
            coords={"band": variances.coords["band"]},
        )

    # square sardeforprob_pdf_width to apply it to variances
    sardeforprob_pdf_width = sardeforprob_pdf_width**2

    # forest and non-forest probabilities
    p_forest = np.exp(multivariate_logpdf(
        medians,
        sardeforprob_pdf_width * variances,
        obs
    ))
    p_nonforest = np.exp(multivariate_logpdf(
        medians + sardeforprob_pdf_shift,
        sardeforprob_pdf_width * variances,
        obs
    ))

    # conditional non-forest probability
    p_cond_nf = p_nonforest / (p_forest + p_nonforest)

    # scale the conditional non-forest probabilities for individual
    # observations (instead of clipping, which is what JReiche did) to
    # prevent too many false positives
    obs_range = 2 * sardeforprob_scaling - 1.0
    p_cond_nf = p_cond_nf * obs_range + (1.0 - sardeforprob_scaling)

    p_change = p_cond_nf.where(valid_obs, np.nan)

    p_change = p_change.assign_attrs(obs_attrs)
    result_dims = [d for d in obs_dims if d != 'band']

    return p_change.transpose(*result_dims)


def change_date(
        *args: DataArray,
        changedate_flag_threshold: float = 0.5,
        changedate_unflag_threshold: float = 0.5,
        changedate_confirm_threshold: float = 0.98,
        changedate_reverse: bool = False,
    ) -> DataArray:
    """Derive the date of change from a series of change probabilities by
    flagging pixels where the pchange exceeds than the
    pchange_threshold_flag and eventually confirming those changes when the
    cumulative evidence (calculated by iterative bayesian updating of the
    change probabilities from the flagging moment onwards) exceeds
    pchange_threshold_confirm.
    If for a pixel the cumulative evidence falls below
    changedate_unflag_threshold instead, that pixel is unflagged.

    Args:
        args (xr.DataArray): tuple with time series of change
            probabilities as DataArrays

    Keyword Args:
        changedate_flag_threshold (float): threshold for change event
            flagging (when raw change probability exceeds this)
        changedate_unflag_threshold (float): threshold for change event
            unflagging (when posterior becomes lower than this)
        changedate_confirm_threshold (float): threshold for change event
            confirmation (when posterior exceeds this)
        changedate_reverse (bool): set to True to use reverse change
            detection.

    Raises:
        RuntimeError: when input has other dimensions than x, y, and time,
            or warm start files are incorrectly specified

    Returns:
        xr.DataArray: array with 3 variables:
            - flagging dates of confirmed pixels
            - confirmation dates of confirmed pixels
            - posterior probability of flagged & confirmed pixels
    """
    # make sure 'band' is a single-valued dimension, and remove it
    p_change = [da.squeeze(dim='band', drop=True)
                if 'band' in da.dims else da for da in args]

    # merge inputs on time dimension
    if len(p_change) > 1:
        p_change = xr.concat(p_change, dim='time')
    else:
        p_change = p_change[0]

    # check if we don't have spurious dimensions, because probably the
    # boolean mask optimization logic goes haywire in that case.
    alloweddims = {"x", "y", "time"}
    if set(p_change.dims) - alloweddims:
        msg = ("change_date() got an input with a dimension other than 'x|y|time'. "
               "Offending dimension(s): {}").format(
                   set(p_change.dims) - alloweddims,
                   )
        raise RuntimeError(msg)

    result_coords = [
        "flag_date",
        "confirm_date",
        "posterior_prob",
    ]

    if p_change.sizes["time"] == 0:
        size_y, size_x = p_change.sizes["y"], p_change.sizes["x"]
        empty = xr.DataArray(
            np.full((size_y, size_x), np.nan),
            dims=["y", "x"],
        )
        result = xr.concat(
            [empty] * 3, dim="change_events",
        ).assign_coords(change_events=result_coords)
        return result

    ascending = True if not changedate_reverse else False
    p_change = p_change.sortby('time', ascending=ascending)

    # convert to y-x multiindex and put it as first dimension so we can
    # use a 1-dimensional boolean mask to do inplace assignments
    p_change = p_change.stack(yx=["y", "x"]).transpose("yx", "time")

    # find first date where prob > flag_treshold, we can skip everything
    # before that
    flag_any_obs = (p_change >= changedate_flag_threshold).any(dim="yx")
    flag_first_idx = flag_any_obs.argmax().item()

    # get initial posterior, and flagged and confirmed masks and date
    # index arrays
    p_change_t0 = p_change.isel(time=flag_first_idx)
    posterior = p_change_t0.where(
        p_change_t0 >= changedate_flag_threshold,
        np.nan
    )
    flagged = posterior >= changedate_flag_threshold
    flag_dates_idx = xr.where(
        flagged,
        flag_first_idx,
        np.nan
    )
    confirmed = posterior >= changedate_confirm_threshold
    confirm_dates_idx = xr.where(
        confirmed,
        flag_first_idx,
        np.nan
    )

    # do iterative bayesian updating
    for time_idx in range(flag_first_idx + 1, p_change.sizes['time']):

        observation = p_change.isel(time=time_idx)

        # make mask for which pixels need updating
        update = ~confirmed & flagged & observation.notnull()

        # calculate new posterior probabilities for those pixels
        previous_sel = posterior[update]
        observation_sel = observation[update]
        postprob_part1 = previous_sel * observation_sel
        postprob_part2 = (1 - previous_sel) * (1 - observation_sel)
        posterior[update] = (postprob_part1 /
                             (postprob_part1 + postprob_part2))

        # flag new pixels where the observed p_change goes above
        # flag_threshold
        new_flags = (~flagged & ~confirmed &
                     (observation >= changedate_flag_threshold))
        if new_flags.any():
            flagged[new_flags] = True
            posterior[new_flags] = observation[new_flags]
            flag_dates_idx[new_flags] = time_idx

        # confirm flagged pixels where the posterior goes above the
        # confirm_threshold
        new_confirmed = ~confirmed & (
            posterior >= changedate_confirm_threshold)
        if new_confirmed.any():
            confirmed[new_confirmed] = True
            confirm_dates_idx[new_confirmed] = time_idx

        # unflag previously flagged but unconfirmed pixels where the
        # posterior goes below unflag_threshold
        unflag = (flagged & ~confirmed &
                  (posterior < changedate_unflag_threshold))
        if unflag.any():
            posterior[unflag] = np.nan
            flag_dates_idx[unflag] = np.nan
            flagged[unflag] = False

    dates = date2mjd(p_change)

    # calculate flag dates
    set_flag_dates = flagged.copy()
    flag_dates = flag_dates_idx * np.nan
    flag_dates_idx = flag_dates_idx.astype(int)
    flag_dates[set_flag_dates] = dates[flag_dates_idx[set_flag_dates]]

    # calculate confirm dates
    set_confirm_dates = confirmed.copy()
    confirm_dates = confirm_dates_idx * np.nan
    confirm_dates_idx = confirm_dates_idx.astype(int)
    confirm_dates[set_confirm_dates] = dates[
        confirm_dates_idx[set_confirm_dates]
    ]

    posterior = posterior.reset_coords("time", drop=True)
    flag_dates = flag_dates.reset_coords("time", drop=True)
    confirm_dates = confirm_dates.reset_coords("time", drop=True)

    posterior = posterior.unstack("yx")
    flag_dates = flag_dates.unstack("yx")
    confirm_dates = confirm_dates.unstack("yx")

    result = xr.concat(
        [flag_dates, confirm_dates, posterior], dim="change_events"
    ).assign_coords(change_events=result_coords)

    return result


def detect_chunk(chunk, offsets, obs_dates, clargs):
    """Run detection for a single chunk.

    Args:
        chunk (np.ndarray): the chunk as returned by sliceDoMp
        offsets (tuple[int, int]): the offsets of this chunk relative to the whole dataset
        obs_dates (list[datetime]): the dates of the observations
        clargs (Namespace): Namespace object with command line args, as created by argparse

    Returns:
        tuple(ndarray), tuple(int): the flag dates, confirm dates, and posterior probabilities
            and the offsets

    """
    mean, var, *obs = chunk
    mean = DataArray(mean, dims=['band', 'y', 'x'])
    var = DataArray(var, dims=['band', 'y', 'x'])
    obs = DataArray(obs, dims=['time', 'band', 'y', 'x'], coords={'time': obs_dates})
    probs = sar_defor_prob(
        obs, mean, var,
        sardeforprob_pdf_width=clargs.defor_pdf_width,
        sardeforprob_pdf_shift=clargs.defor_pdf_shift,
        sardeforprob_scaling=clargs.defor_prob_scaling,
    )
    change_dates = change_date(
        probs,
        changedate_flag_threshold=clargs.flag_threshold,
        changedate_unflag_threshold=clargs.unflag_threshold,
        changedate_confirm_threshold=clargs.confirm_threshold,
    )
    flagged = change_dates.sel(change_events='flag_date').values
    confirmed = change_dates.sel(change_events='confirm_date').values
    postprob = change_dates.sel(change_events='posterior_prob').values
    return (flagged, confirmed, postprob), offsets


def detect_chunk_callback(result, out_ds):
    """Callback function to write the results per chunk

    Args:
        result (tuple[ndarray, tuple[int, int]]): result from detect_chunk
        out_ds (gdal.Dataset): the dataset to write into
    """
    data, offsets = result
    flagged, confirmed, postprob = data
    xoff, yoff = offsets
    gdal_array.BandWriteArray(out_ds.GetRasterBand(1), confirmed, xoff=xoff, yoff=yoff)
    gdal_array.BandWriteArray(out_ds.GetRasterBand(2), flagged, xoff=xoff, yoff=yoff)
    gdal_array.BandWriteArray(out_ds.GetRasterBand(3), postprob, xoff=xoff, yoff=yoff)
