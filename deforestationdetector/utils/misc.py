#!/usr/bin/python3
# -*- coding: utf-8 -*-
# (c) Satelligence, see LICENSE.txt.

import math
import os.path
from typing import Union

import iso8601
import numpy as np
import xarray as xr
from xarray import DataArray


def date_from_filename(filename):
    """Try to parse datetime from filename

    Args:
        filename (str): the filename

    Returns:
        datetime: the parsed datetime
    """
    punctuation = '!"#$%&\'()*+,;<=>?@[\\]^_`{|}~'
    filename, _ = os.path.splitext(os.path.basename(filename))
    trans = filename.maketrans(punctuation, ' ' * len(punctuation))
    words = filename.translate(trans).strip().split()
    for word in words:
        try:
            parsed_date = iso8601.parse_date(word)
            return parsed_date
        except iso8601.ParseError:
            pass
    raise RuntimeError(f"Could not parse date in filename: {filename}.")


def dates_from_stack(dss):
    """Get dates from all files in list of datasets

    Args:
        dss (list): list of datasets

    Returns:
        list[datetime]: list of datetimes
    """
    stack_filenames = [ds.GetFileList()[0] for ds in dss]
    stack_dates = [date_from_filename(f) for f in stack_filenames]
    return stack_dates


def worldToPixel(gt, x, y, integer=False, center=False):
    """Convert projected to pixel coordinates

    Args:
        gt (tuple): geotransform, as returned by gdal's Dataset.GetGeoTransform()
        x (float): x coordinate
        y (float): y coordinate
        integer (bool): cast result to integer
        center (bool): use pixel center instead of upper-left corner

    Returns:
        tuple: the pixel coordinates
    """
    if center:
        shift = 0.5
    else:
        shift = 0
    px = ((x - gt[0]) / gt[1]) - shift
    py = ((y - gt[3]) / gt[5]) - shift
    if integer:
        px = int(px)
        py = int(py)
    return (px, py)


def pixelToWorld(gt, px, py, center=False):
    """Convert pixel to projected coordinates

    Args:
        gt (tuple): geotransform, as returned by gdal's Dataset.GetGeoTransform()
        px (float): x coordinate
        py (float): y coordinate
        center (bool): use pixel center instead of upper-left corner

    Returns:
        tuple: the projected coordinates
    """
    if center:
        shift = 0.5
    else:
        shift = 0
    wx = gt[0] + (px + shift) * gt[1]
    wy = gt[3] + (py + shift) * gt[5]
    return (wx, wy)


def list_take(l1, l2):
    """Pick selected indices from a list

    Args:
        l1 (list): the list to pick from
        l2 (list): list with indices of items that should be picked

    Returns:
        list: subset of l1, defined by indices in l2
    """
    l3 = []
    for i in l2:
        l3.append(l1[i])
    return l3


def unpad(a, pad):
    """Remove padding from last 2 dimensions of a numpy array

    Args:
        a (np.ndarray): the array to unpad
        pad (int): the (uniform) padding that should be removed

    Returns:
        np.ndarray: array with padding removed from the last 2 dimensions
    """
    if pad > 0:
        return a[...,pad:-pad, pad:-pad]
    else:
        return a


def logpdf(
        mean: Union[DataArray, np.ndarray, float],
        variance: Union[DataArray, np.ndarray, float],
        samples: Union[DataArray, np.ndarray, float]) -> DataArray:
    """Calculate single-variate gaussian logpdf for x given mean and variance.

    Args:
        mean (xarray.DataArray): mean, same dims as samples
        variance (xarray.DataArray): variance, same dims as samples
        samples (xarray.DataArray): values

    Returns:
        xarray.DataArray: logpdf for each sample given its mean and variance
    """
    result = (-0.5 * math.log(2 * math.pi)
              - 0.5 * np.log(variance)
              - 0.5 * (samples - mean) ** 2 / variance)

    return result


def maxlogpdf(variance: Union[DataArray, np.ndarray, float]) -> DataArray:
    """Calculate the max logpdf of a gaussian with variance is vars

    Args:
        variance (xarray.DataArray): variances

    Returns:
        xarray.DataArray: maximum logpdf values
    """
    result = -0.5 * math.log(2 * math.pi) - 0.5 * np.log(variance)

    return result


def multivariate_logpdf(
        means: DataArray,
        variances: DataArray,
        observations: DataArray,
        dim: str = 'band',
    ) -> DataArray:
    """NaN-ignoring multi-variate gaussian logpdf for diagonal covariance matrices.

    see: https://stackoverflow.com/questions/48686934/numpy-vectorization-of
    -multivariate-normal

    Args:
        means (xarray.DataArray): mean vectors, same dims and shape as observations
        variances(xarray.DataArray): variance vectors, same dims and shape as observations
        observations (xarray.DataArray): observations data
        dim (str): dimension along which to apply function

    Returns:
        xarray.DataArray: logpdfs of observations
    """
    nfeatures = observations.sizes[dim]
    if nfeatures == 1:
        # use faster single-variate calculation if there is just 1 feature
        result = logpdf(means, variances, observations)
        # in the multivariate code path below, 'dim' is automatically dropped.
        # To maintain consistency, also drop it here
        if dim in result.dims:
            result = result.squeeze(dim=dim, drop=True)
    else:
        constant = xr.full_like(
            observations, np.log(2 * np.pi)).where(observations.notnull(), np.nan)
        log_determinants = np.log(variances.where(variances.notnull(), np.nan).prod(dim=dim))
        deviations = observations - means
        inverses = 1.0 / variances.where(variances.notnull(), np.nan)
        valid = observations.notnull().any(dim=dim)

        result = -0.5 * (
            log_determinants + (
                constant + deviations * inverses * deviations).sum(dim=dim)).where(valid, np.nan)

    return result


def multivariate_maxlogpdf(
        variances: DataArray,
        dim: str = 'band'
    ) -> DataArray:
    """NaN-ignoring multi-variate gaussian maxlogpdf for diagonal covariance
    matrices.

    Args:
        variances (xarray.DataArray): variance vectors
        dim (str): dimension along which to apply function

    Returns:
        xarray.DataArray: maxlogpdfs of multivariate distributions
    """
    nfeatures = variances.sizes[dim]

    if nfeatures == 1:
        # use faster single-variate calculation if there is just 1 feature
        result = maxlogpdf(variances)
        # in the multivariate code path below, 'dim' is automatically dropped.
        # To maintain consistency, also drop it here
        if dim in result.dims:
            result = result.squeeze(dim=dim, drop=True)
    else:
        constant = xr.full_like(variances, np.log(2 * np.pi)).where(variances.notnull(), np.nan)
        log_determinants = np.log(variances.where(variances.notnull(), np.nan).prod(dim=dim))
        valid = variances.notnull().any(dim=dim)

        result = -0.5 * (log_determinants + (constant).sum(dim=dim)).where(valid, np.nan)

    return result


def date2mjd(data: DataArray) -> DataArray:
    """Modified Julian dates in days since 1970-01-01

    Calculate the modified Julian 1970 dates for data, as float

    Args:
        data (np.array or xr.DataArray): the data to extract Julian dates from

    Returns:
        np.array or xr.DataArray: array with julian date
    """
    if isinstance(data, DataArray) and 'time' in data.coords:
        dates = data.coords['time']
    else:
        dates = np.asarray(data)
    mjd = (dates.astype(np.datetime64) - np.datetime64('1970')) / np.timedelta64(1, 'D')
    return mjd
