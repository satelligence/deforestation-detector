#!/usr/bin/python3
# -*- coding: utf-8 -*-
# (c) Satelligence, see LICENSE.txt.

import argparse

import numpy as np
from osgeo import gdal

from utils.detection import detect_chunk, detect_chunk_callback
from utils.inout import create_dataset, StackedDataset, sliceDataset, sliceDoMp
from utils.misc import dates_from_stack, date_from_filename


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--stats-mean', type=str, required=True, help='path to historical mean raster')
    parser.add_argument('--stats-var', type=str, required=True, help='path to historical variance raster')
    parser.add_argument('--out', type=str, required=True, help='path to output file')
    parser.add_argument('--defor-pdf-width', type=float, nargs='+', default=2.0, help='historical pdf width scaling factor(s)')
    parser.add_argument('--defor-pdf-shift', type=float, nargs='+', default=-2.0, help='historical pdf shift factor(s)')
    parser.add_argument('--defor-prob-scaling', type=float, default=0.95, help='deforestation probability scaling to prevent single observation confirmations')
    parser.add_argument('--flag-threshold', type=float, default=0.5, help='change detection flagging threshold')
    parser.add_argument('--unflag-threshold', type=float, default=0.5, help='change detection unflagging threshold')
    parser.add_argument('--confirm-threshold', type=float, default=0.98, help='change detection confirmation threshold')
    parser.add_argument('--obs', nargs='+', help='raster files with observations')
    args = parser.parse_args()
    mean_filename = args.stats_mean
    var_filename = args.stats_var
    obs_filenames = args.obs
    mean_var_obs_dss = [gdal.Open(ds) for ds in [mean_filename, var_filename, *obs_filenames]]
    obs_dates = dates_from_stack(mean_var_obs_dss[2:])
    mean_var_obs_stack = StackedDataset(mean_var_obs_dss, mode='first')
    out_ds = create_dataset(args.out, prototype=mean_var_obs_dss[0], nbands=3)

    sliceDoMp(
        sliceDataset(mean_var_obs_stack, pad_fill=np.nan),
        detect_chunk,
        mp_function_args=(obs_dates, args),
        callback=detect_chunk_callback,
        callback_args=(out_ds,)
    )
