# Deforestation Detector

Detect deforestation from satellite data timeseries by comparing observations with a 
predefined historical mean and variation. Observations are translated to deforestation 
probabilities, given the historical statistics. Observations that pass a probability 
threshold are flagged, and subsequent observations are added in a bayesian manner 
('bayesian updating'). If the posterior probability of the bayesian updating passes 
the confirmation threshold, a detection is made and the flagging and confirmation date 
for that pixel are saved. If on the other hand the posterior probability after updating 
goes down again and crosses the unflagging threshold, the flagging event was probably an 
anomaly, and the flagging is canceled.

Implementation is based on [this paper](https://www.mdpi.com/2072-4292/10/5/777) and was done in the scope of 
the [Radar Alerts for Detecting Deforestation](https://www.wri.org/news/release-palm-oil-industry-jointly-develop-radar-monitoring-technology-detect-deforestation).
 Deforestationdetector implements the methods developed by Wageningen University 
in a scalable way, making it possible to apply it to arbitrary large datasets with 
good computational performance.

## Installation

Clone this repository. The application can run as-is, without installation, 
by invoking the main script `deforestationdetector/deforestationdetector.py` 
directly, or can be installed by running `python3 -m pip install .` from the 
`deforestationdetector` folder. This will make the `deforestationdetector.py` 
application available without specifying the path.

## Requirements

Deforestationdetector.py is written in python3, so it needs python3 installed. 
All other requirements should be installed by pip when installing locally. 

In case you do not want to install locally but run directly from the source folder, 
these python packages should be installed:
- dateparser
- gdal
- numpy
- xarray

You can also install the requirements for example in a virtual environment
with `pip3 install -r requirements.txt`

For the gdal python package to work, the gdal library also needs to be 
installed (this will not be done by pip).

## Usage

```deforestationdetector.py [-h] --stats-mean STATS_MEAN --stats-var STATS_VAR --out OUT [--defor-pdf-width DEFOR_PDF_WIDTH [DEFOR_PDF_WIDTH ...]] [--defor-pdf-shift DEFOR_PDF_SHIFT [DEFOR_PDF_SHIFT ...]] [--defor-prob-scaling DEFOR_PROB_SCALING] [--flag-threshold FLAG_THRESHOLD] [--unflag-threshold UNFLAG_THRESHOLD] [--confirm-threshold CONFIRM_THRESHOLD] [--obs OBS [OBS ...]]```

The application will construct a historical probability-density function (pdf) per pixel, 
using the historical mean and variance data. 
These pdfs are assumed to represent the undisturbed, forested state. 

Then, using the `defor-pdf-shift` and `defor-pdf-width` parameters, 
per pixel an artificial non-forest pdf will be constructed, by scaling the 
forest pdf's variance(s) with `defor-pdf-width`, and by shifting the mean(s) 
with `defor-pdf-shift`. This represents the non-forest state. Every observation 
will be compared to both pdf's, and the probability that the observation belongs 
to the non-forest pdf is calculated. If this probability exceeds the `flag-threshold`, 
subsequent observations are added by bayesian updating, thus strengthening 
the evidence if the probability > 0.5, or weakening it if it is < 0.5. 
Once this posterior probability exceeds `confirm-threshold`, the pixel is 
believed to be deforested and the flag and confirmation dates are saved. 
If, on the other hand, the posterior probability goes down below the `unflag-threshold`, 
the flagging was probably due to an anomaly, and the detection continues.

All calculations are done in a tilewise manner, running parallel on as many cores as 
are present. This allows arbitrary large areas to be processed without memory issues.

### Required command line arguments

`--stats-mean`: path to the raster file with the historical means. 
New observations will be compared to the historical pdf and the artificial deforestation pdf, 
to calculate the probability that they belong to the deforestation class. 
The `stats-mean` raster file can have multiple bands, in that case the `stats-var` and 
observations should also have multiple bands, and a multivariate pdf will be constructed. 
Any gdal-readable file type can be used, as long as it has proper georeferencing information. 
The bounding box of the calculations and of the result is set to the extent of the stats-mean 
file, so this can be used to limit the calculations and output to a certain bounding box.

`--stats-var`: path to the raster file with historical variances. Must have the same number 
of bands as the file with historical means. Any gdal-readable file type can be used, as long 
as it has proper georeferencing information.

`--out`: path to the output (tif) file. NB it will be replaced without warning if it already 
exists!

`--obs`: list of files with observations. Need to have the same number of bands as mean and 
variance above. Need to be gdal-readable. Need to be floating point files, and nodata values must be set to NaN.
Observation filenames need to have a parsable date in the filename. The [pyiso8601 module](https://pyiso8601.readthedocs.io/en/latest/) is used for date parsing, see there for acceptable date(time) formats. Something like '000164_ascending_2020-12-30.tif' or 'S1A_IW_GRDH_1SDV_20210204T181807_20210204T181836_036444_044730_5625' will work (the first parsable date that is found in the filename will be used). 
The extent of these files need not be the same, nor does it need to match the extent of the 
mean/variance files. The intersection with the extent of the mean file will automatically be 
used; parts that fall out of the bounding box of the mean file will be ignored, and missing 
parts will be filled with nodata.

### optional command line arguments

`--defor-pdf-width`: scaling factor for the variance of the pdf. 
Can be a single value (will be used for all bands), or multiple values, 
one per band.

`--defor-pdf-shift`: shift factor for the mean of the pdf. 
Can be a single value (will be used for all bands), 
or multiple values, one per band.

`--defor-prob-scaling`: scale down the calculated non-forest probabilities 
to avoid single observations triggering a direct confirmation.

`--flag-threshold`: non-forest probability threshold for flagging an observation.

`--unflag-threshold`: non-forest probability unflagging threshold.

`--confirm-threshold`: posterior probability confirmation threshold.

### outputs

The result of the detection is written to a geotiff file, specified with `--out`. This file contains 3 bands, of float32 type. First band is the flag date, in days since 1970-01-01T00:00:00.0, for every flagged pixel. The second band is the confirmation data, in the same units, for every confirmed pixel. The third band contains the non-forest posterior probabilities at the last observation date.

Flag and confirmation dates are given in days since 1970-01-01 because that is much easier to use for calculations and for storage. Also, for visualization purposes, it gives a linear scale through time, which would not be the case when a encoding like yyyy.ddd was used, which would have gaps for every y.366 until the next year.
